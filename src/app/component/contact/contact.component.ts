import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent {
  // constructor() {}
  // ngOnInit(): void {}
  ContactForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    dob: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    hobby: new FormControl(''),
    gender: new FormControl(''),
    comment: new FormControl('', [Validators.required]),
  });

  contactData() {
    console.warn(this.ContactForm.value);
  }

  get name() {
    return this.ContactForm.get('name');
  }
  get email() {
    return this.ContactForm.get('email');
  }
  get dob() {
    return this.ContactForm.get('dob');
  }
  get city() {
    return this.ContactForm.get('city');
  }
  get comment() {
    return this.ContactForm.get('comment');
  }
}
